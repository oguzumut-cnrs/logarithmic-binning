

string read_from_a_file(std::vector<double>& m)
{
	ifstream metrics,disp;
	string filename2;
	double th,scaling,base;
	int imin,row_number;

	
	
// 	filename2="strain_dissip.dat";

    cout << "Please enter filename: ";
    getline(cin, filename2);
	cout<<"reading file "<<filename2<<endl;

    cout << "Please enter upper or lower 	threshold for avalanche, well number or alpha: ";
    cin>> th;
	cout<<"smallest value to begin with "<<th<<endl;
	
    cout << "Please enter starting line  for reading: ";
    cin>> imin;
	cout<<"starting line  for reading "<<imin<<endl;
	
	
// 	cout << "Please enter scaling factor: ";
//     cin>> scaling;
// 	cout<<"scaling factor: "<<scaling<<endl;
	scaling = 1.;

	cout << "Please enter setup: ";
    cin>> row_number;
	cout<<"setup: "<<row_number<<endl;


	
	ifstream file;
	file.open(filename2.c_str());



	string line;
	int count=0;
	while (getline(file, line))
	count++;
	cout << "Numbers of lines in the file : " << count << endl;
	file.close();



	double temp,temp2;

	//    cout<< filename << "  \n";
	disp.open(filename2.c_str());

	m.resize( 0 );
	std::vector<double> temp_v(0);
	temp_v.resize(8);

	if(row_number ==1){
		for (int i=0;i<count;++i)
		{
			disp >>temp;
			if(temp > th && i >= imin && temp > 0.)
				m.push_back(temp/scaling);		
		}
	}


	else if(row_number ==2){
		double th_second=0;
		double th_third=0;

		cout << "Please enter the  threshold for the second column data: ";
    	cin>> th_second;
		cout<<"the threshold for the second column data "<<th_second<<endl;

	
		for (int i=0;i<count;++i)
		{
			disp >>temp >>temp2;
			if(temp > th && i >= imin && temp2 > th_second)
				m.push_back(temp2/scaling);		
		}
	}

// 	else if(row_number ==3){
// 		double th_second=0;
// 		double th_third=0;
// 
// 		cout << "Please enter the threshold for the second column data: ";
//     	cin>> th_second;
// 		cout<<"the threshold for the second column data "<<th_second<<endl;
// 
// 	
// 		for (int i=0;i<count;++i)
// 		{
// 			disp >>temp >>temp2;
// 			if(temp < th && i >= imin && temp2 > th_second)
// 				m.push_back(temp2/scaling);		
// 		}
// 	}


	else if(row_number ==3){
		double th_second=0;
		double th_third=0;

		cout << "Please enter the threshold for the stress(8) value: ";
    	cin>> th_second;
		cout<<"the threshold  for the well value "<<th_second<<endl;

// 		int analyze;
// 		cout << "analyze stress(3) or energy (4): ";
//     	cin>> analyze;
// 		cout<<"analyzing coloumn "<<analyze<<endl;

		cout << "Please enter the  threshold for the second column data: ";
    	cin>> th_third;
		cout<<"the threshold for the second column data "<<th_third<<endl;

		int ii;
		cout << "Please enter the  column number to be analyzed: ";
    	cin>> ii;
		cout<<"column number to be analyzed "<<ii<<endl;
		ii=ii-1;

		double welln;
		double temp3,temp4,temp5,temp6,temp7,temp8,dumb;
		for (int i=0;i<count;++i){
// 			cout<<"at line "<<i<<endl;
			disp >>temp >>welln >>temp3 >>temp4 >>temp5 >>temp6 >>temp7>>temp8
			 >>dumb >>dumb >>dumb >>dumb>>dumb >>dumb;
			temp_v[0]  = temp;
			temp_v[1]  = welln;
			temp_v[2]  = temp3;
			temp_v[3]  = temp4;
			temp_v[4]  = temp5;
			temp_v[5]  = temp6;
			temp_v[6]  = temp7;
			temp_v[7]  = temp8;
			if(temp > th && temp8 > th_second && temp_v[ii]>th_third)
				m.push_back(temp_v[ii]);
 		}
	}
	


	else if(row_number ==4){
		double th_second=0;
		double th_third=0;

		cout << "Please enter the threshold for the well value: ";
    	cin>> th_second;
		cout<<"the threshold  for the well value "<<th_second<<endl;

// 		int analyze;
// 		cout << "analyze stress(3) or energy (4): ";
//     	cin>> analyze;
// 		cout<<"analyzing coloumn "<<analyze<<endl;

		cout << "Please enter the  threshold for the second column data: ";
    	cin>> th_third;
		cout<<"the threshold for the second column data "<<th_third<<endl;

		int ii;
		cout << "Please enter the  column number to be analyzed: ";
    	cin>> ii;
		cout<<"column number to be analyzed "<<ii<<endl;


		double welln;
		double temp3,temp4,temp5,temp6,temp7,temp8;
		for (int i=0;i<count;++i){
// 			cout<<"at line "<<i<<endl;
			disp >>temp >>welln >>temp3 >>temp4 >>temp5 >>temp6 >>temp7>>temp8;
			temp_v[0]  = temp;
			temp_v[1]  = welln;
			temp_v[2]  = temp3;
			temp_v[3]  = temp4;
			temp_v[4]  = temp5;
			temp_v[5]  = temp6;
			temp_v[6]  = temp7;
			temp_v[7]  = temp8;
			if(temp < th && welln > th_second && temp_v[ii]>th_third)
				m.push_back(temp_v[ii]/scaling);
 		}
	}
	
	disp.close();
	
	cout<<"max new  ="<<*max_element(m.begin(), m.end()) <<endl;
	cout<<"number of avalanches  ="<<m.size()<<endl;

      fstream cleaned_file;
      string logbinfilename = "cleaned.dat"; 

	  cleaned_file.open(logbinfilename , fstream::in | fstream::out | fstream::trunc);

     for(int i=0;i<m.size();i++)
        cleaned_file<<std::scientific << std::setprecision(7)<< m[i]<<endl;

		
	cleaned_file.close();
	return filename2;
	
	

}


double maxval(std::vector<double>& m)
{
    double max;
    max  = *max_element(m.begin(), m.end());
    return max;

}

double minval(std::vector<double>& m)
{
    double min;
    min  = *min_element(m.begin(), m.end());
    return min;

}

double sum_array(std::vector<double>& m)
{
	double temp = std::accumulate( m.begin(),m.end(), 0.0);
	return temp;
}


bool logarithmic_binning_std(std::vector<double>& m, int& n_bin, string& filename_user, double base)
{
	bool  check=0;
	std::vector<double> density,xx,error,edges;
	std::vector<int> density_int;

	density.resize(n_bin);
	xx.resize(n_bin);
	error.resize(n_bin);
	edges.resize(n_bin+1);
	density_int.resize(n_bin);

	xx.assign (n_bin,0);
	density.assign (n_bin,0); // n_bin doubles with a value of 0
	error.assign (n_bin,0); // n_bin doubles with a value of 0
	edges.assign (n_bin+1,0); // n_bin doubles with a value of 0
	density_int.assign (n_bin,0); // n_bin doubles with a value of 0

	cout<<"max of density check = "<<maxval(density)<<endl;
	cout<<"max of density check = "<<maxval(error)<<endl;
	
	
	cout<<" max m "<<maxval(m)<<endl;
	cout<<" min m "<<minval(m)<<endl;
	
	if( m.size() <= n_bin )
	{
		cout<<"meaningless input; not enough number of values in the file"<<endl;
		check =0;
		return check; 
	
	}
	if( minval(m) <= 0. )
	{
	  cout<<"Le minimum de la fonction Y=f(X) ds (',n_min,n_max,') est < 0:  min = "<<minval(m)<<endl;
	  cout<<"On ne peut donc pas calculer son Log !! "<<endl;
	  check =0;
	  return check;
	}
//   
// 	
//   
// 
// // !------------- calcul de la loi de proba de log( fct(:) ) -----------------
//      //std::sort(&m[0],&m[0]+m.size());
     std::sort(m.begin(), m.end());
     
// 
//      
	std::vector<double> m_real;
	m_real.resize(m.size());
     for(int i=0;i<m.size();i++){
     	m_real[i] =m[i];
     	m[i] =  doublelog(base,m[i]);
     }
	 double r_min = minval(m);
	 double r_max = maxval(m);
     
     double zero=1.e-10;
             
     double div = ( r_max - r_min ) / n_bin; 
    
     for(int i=0;i<m.size();i++)
     {
     	int n_div = int( ( m[i]-r_min ) / div - zero ) ;
     	density[n_div]++; 
     	density_int[n_div]++; 
 	
     }
   
    for(int i=0;i<density.size();i++)
    	density[i] /= div*m.size();
    	
    double test = sum_array(density) * div;
    cout<<"Test sur la normalisation de la densite de proba de Log[f(t)]              :  norme = "<<test<<endl;


//  !------ calcul de la loi de proba de fct(:) --------

      double fact = 1. / std::log(base);
      //double fact = 1. / log(10);
      //fact = 1.;
      fstream logbin;
      string logbinfilename = "logbin_" + filename_user + "_.dat"; 

	  logbin.open (logbinfilename , fstream::in | fstream::out | fstream::trunc);
//        ofs.open("test.txt", std::ofstream::out | std::ofstream::trunc);

      for(int i=0;i<n_bin;i++)
      {
          //xx[i]      = exp( r_min + (i)*div + div/2. )  ;
         //xx[i]      = pow(10.,( r_min + (i)*div + div/2. ) ) ;
         xx[i]      = pow(base,( r_min + ((double) i)* (double) div + (double)  	div/2. ) ) ;
         density[i] = fact * density[i]   / xx[i];
         if( density[i] > 0. ) 
         	//logbin<<std::scientific << std::setprecision(7)<< std::log(xx[i])<<" "<< std::log(density[i])<<" "<<endl;
         	logbin<<std::scientific << std::setprecision(7)<< doublelog(base,xx[i])<<" "<< doublelog(base,density[i])<<" "<<endl;
      }
      fstream fsedges,fsdensity;
      string edgesbinfilename = "edges" + filename_user + "_.dat"; 
      fsedges.open (edgesbinfilename , fstream::in | fstream::out | fstream::trunc);
      string fsdensityfilename = "density" + filename_user + "_.dat"; 
      fsdensity.open (fsdensityfilename , fstream::in | fstream::out | fstream::trunc);

     for(int i=0;i<=n_bin;i++)
      {
        // edges[i] = pow(10.,( r_min + (i)*div) ) ;
        // 	edges[i] = exp( r_min + (i)*div)  ;
         	edges[i] = pow(base,( r_min + ((double) i)* (double) div) ) ;
         	fsedges<<std::scientific << std::setprecision(7)<< edges[i]<<endl;

      }      
     for(int i=0;i<n_bin;i++)
      {
         	fsdensity<<std::scientific << std::setprecision(7)<<density_int[i]<<endl;

      }          
	  logbin.close(); 
	  cout<<"FILE READY: "<<logbinfilename <<endl;
	  fstream loglikely;
	  string likelyfilename = "loglikely_" + filename_user + "_.dat"; 
// 	  loglikely.open ("loglikely.dat", fstream::in | fstream::out | fstream::app);
	  loglikely.open (likelyfilename, fstream::in | fstream::out | fstream::trunc);

// 	  std::sort(&m(0),&m(0)+m.size());
	  double N= (double) m.size();

	  for(int i=0;i<m.size();i++)
	  {
	  	//loglikely<<std::scientific << std::setprecision(7)<< m[i]<<" "<<  std::log(    (N-(double) i) /   (double) m.size())<<endl;
	  	loglikely<<std::scientific << std::setprecision(7)<< m[i]<<" "<<  
	  	doublelog(base,  ((double) N-(double) i) /   (double) m.size())<<endl;

	  }    
	  check =1;
	  loglikely.close();  
	  cout<<"FILE READY: "<<likelyfilename <<endl;
	  
	  
	  std::vector<double> newberry;
	  newberry.resize(m.size());

	  for(int i=0;i<m_real.size();++i)
	  	newberry[i] = 0.;

	  	
	  double sumtemp=0;
	  double x_min = minval(m_real);

	  
	  for(int i=0;i<newberry.size();++i)
	  	newberry[i] =  x_min * pow(base,floor(doublelog(base,m_real[i]/x_min)));
	  for(int i=0;i<newberry.size();++i)
	  	sumtemp += doublelog(base,newberry[i]/x_min ) ;	
	  
	  sumtemp /=m_real.size();
	  cout << "divided sum= "<<sumtemp <<std::endl;
	  double sum = pow(sumtemp,-1.) ;
	  	
	  double exponent = doublelog(base,1.+sum);
	   
	  cout<<"PRL(newberry) exponent: "<<1.+exponent <<endl;


	 return check;
	
	
}