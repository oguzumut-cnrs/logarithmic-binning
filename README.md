The function “logarithmic_binning_std(std::vector& m, int& n_bin)” takes m (std:vector holding the data to bin) and n_bin (number of bins required by user) as arguments, operates a logarithmic binning to calculate probability density function (PDF) on m and returns the results on a log-log plot in the file “logbin.dat”. It also returns the (complementary) cumulative distribution (cdf) on a log-log plot in the file “loglikely.dat”, (see https://en.wikipedia.org/wiki/Power_law for the definition of CDF). One can then compare PDF et CDF. It can be useful in the study of power law distributions.

 

With the new interface added, now you can enter the filename (column file) , the minimum avalanche size to be included in the calculation of PDF et CDF, the first line number from where code starts to read and number of bins.
