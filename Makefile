# This is the makefile used to build the dlib C++ library's regression test suite
# on Debian Linux using the gcc compiler.

# this is the name of the output executable
TARGET =  ~/bin/cpp_av.exe

X11ROOT = /usr/X11

# these are the compile time flags passed to gcc
# CXXFLAGS ?= -ggdb -Wall
CXXFLAGS ?= -ggdb 


# CPPFLAGS ?=  -std=c++11     -O2  -I/Users/salman/boost_1_62_0 
# CPPFLAGS ?=  -std=c++11     -O2  -I/usr/local/opt/boost/include  -I./dlib-19.1  -I ./fftw++-2.03 -I ./fftw++-2.03/fftw++.cc -I/usr/local/include 
CPPFLAGS ?=  -std=c++11     -O2  

# -I./source/utils is required for intel daal
# These are the link time flags passed to gcc
#the commented line is required for intel daal
LFLAGS = -lpthread     -L/usr/X11/lib    -lX11  

 
#
# The name of the compiler.  If you only have one version of
# gcc installed then you probably want to change this to just g++ 
CXX ?=  clang

####################################################
####################################################
#  Here we list all the cpp files we want to compile

SRC = main.cpp


####################################################

TMP = $(SRC:.cpp=.o)
OBJ = $(TMP:.c=.o)

$(TARGET): $(OBJ) 
	@echo Linking $@
	@$(CXX) $(LDFLAGS) $(OBJ) $(LFLAGS) -o $@
	@echo Build Complete

clean:
	@rm -f $(OBJ) $(TARGET)
	@echo All object files and binaries removed

dep: 
	@echo Running makedepend
	@makedepend -- $(CFLAGS) -- $(SRC) 2> /dev/null 
	@echo Completed makedepend


